package com.unit6.easen.tester.domain;

import com.unit6.easen.tester.domain.results.TestResult;

import java.util.List;

public interface ResultsStorage {

    public List<TestResult> getAllResults();

    public void addResult(TestResult result);

    public void removeResult(TestResult result);

    public void removeAll();
}
