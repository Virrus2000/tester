package com.unit6.easen.tester.domain.results;

import com.unit6.easen.tester.domain.results.operation.FIOOperation;

import java.io.Serializable;
import java.util.Map;

public class FIOJob implements Serializable{

    private String jobname;
    private int groupid;
    private int error;
    private FIOOperation read;
    private FIOOperation write;
    private FIOOperation trim;
    private float usr_cpu;
    private float sys_cpu;
    private int ctx;
    private int majf;
    private int minf;
    private Map<String,String> iodepth_level;
    private Map<String,String> latency_us;
    private Map<String,String> latency_ms;
    private int latency_depth;
    private int latency_target;
    private float latency_percentile;
    private int latency_window;

    public FIOJob() {
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public FIOOperation getRead() {
        return read;
    }

    public void setRead(FIOOperation read) {
        this.read = read;
    }

    public FIOOperation getWrite() {
        return write;
    }

    public void setWrite(FIOOperation write) {
        this.write = write;
    }

    public FIOOperation getTrim() {
        return trim;
    }

    public void setTrim(FIOOperation trim) {
        this.trim = trim;
    }

    public float getUsr_cpu() {
        return usr_cpu;
    }

    public void setUsr_cpu(float usr_cpu) {
        this.usr_cpu = usr_cpu;
    }

    public float getSys_cpu() {
        return sys_cpu;
    }

    public void setSys_cpu(float sys_cpu) {
        this.sys_cpu = sys_cpu;
    }

    public int getCtx() {
        return ctx;
    }

    public void setCtx(int ctx) {
        this.ctx = ctx;
    }

    public int getMajf() {
        return majf;
    }

    public void setMajf(int majf) {
        this.majf = majf;
    }

    public int getMinf() {
        return minf;
    }

    public void setMinf(int minf) {
        this.minf = minf;
    }

    public Map<String, String> getIodepth_level() {
        return iodepth_level;
    }

    public void setIodepth_level(Map<String, String> iodepth_level) {
        this.iodepth_level = iodepth_level;
    }

    public Map<String, String> getLatency_us() {
        return latency_us;
    }

    public void setLatency_us(Map<String, String> latency_us) {
        this.latency_us = latency_us;
    }

    public Map<String, String> getLatency_ms() {
        return latency_ms;
    }

    public void setLatency_ms(Map<String, String> latency_ms) {
        this.latency_ms = latency_ms;
    }

    public int getLatency_depth() {
        return latency_depth;
    }

    public void setLatency_depth(int latency_depth) {
        this.latency_depth = latency_depth;
    }

    public int getLatency_target() {
        return latency_target;
    }

    public void setLatency_target(int latency_target) {
        this.latency_target = latency_target;
    }

    public float getLatency_percentile() {
        return latency_percentile;
    }

    public void setLatency_percentile(float latency_percentile) {
        this.latency_percentile = latency_percentile;
    }

    public int getLatency_window() {
        return latency_window;
    }

    public void setLatency_window(int latency_window) {
        this.latency_window = latency_window;
    }
}

