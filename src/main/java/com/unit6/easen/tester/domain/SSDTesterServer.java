package com.unit6.easen.tester.domain;

import com.unit6.easen.tester.domain.results.TestResult;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public interface SSDTesterServer {

    public void login() throws ServerInteractionException;

    public void send(TestResult result) throws ServerInteractionException;

    public void logout() throws ServerInteractionException;

    public String getParams();
}
