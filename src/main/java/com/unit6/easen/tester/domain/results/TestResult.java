package com.unit6.easen.tester.domain.results;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestResult implements Serializable {

    private Date testStart;
    private long testDuration;
    private List<String> results = new LinkedList<>();

    public TestResult(Date testStart) {
        this.testStart = testStart;
    }

    public void setTestDuration(long testDuration) {
        this.testDuration = testDuration;
    }

    public void addNewResult(String result){
        results.add(result);
    }

    public Date getTestStart() {
        return testStart;
    }

    public long getTestDuration() {
        return testDuration;
    }

    public List<String> getResults() {
        return results;
    }
}
