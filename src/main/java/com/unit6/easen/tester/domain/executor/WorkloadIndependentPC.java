package com.unit6.easen.tester.domain.executor;

public class WorkloadIndependentPC {

    private long capacity;
    private TestParameter testParameters;

    public void setTestParameters(TestParameter testParameters) {
        this.testParameters = testParameters;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public void execute() {

    }
}
