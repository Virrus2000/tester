package com.unit6.easen.tester.domain.executor;

public class RWMix {

    private final int read;
    private final int write;

    public RWMix(int read, int write) {
        this.read = read;
        this.write = write;
        if (this.read + this.write != 100) {
            throw new IllegalArgumentException("incorrect percentage, read+write must be 100");
        }
    }

    public int getRead() {
        return read;
    }

    public int getWrite() {
        return write;
    }

    public static final RWMix read(int read) {
        if (read > 100 || read < 0) {
            throw new IllegalArgumentException("read percentage must be 0..100");
        }
        return new RWMix(read, 100 - read);
    }

    public static final RWMix write(int write) {
        if (write > 100 || write < 0) {
            throw new IllegalArgumentException("write percentage must be 0..100");
        }
        return new RWMix(100 - write, write);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RWMix)) return false;

        RWMix rwMix = (RWMix) o;

        if (read != rwMix.read) return false;
        if (write != rwMix.write) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = read;
        return result;
    }
}

