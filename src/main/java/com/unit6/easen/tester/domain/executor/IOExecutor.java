package com.unit6.easen.tester.domain.executor;

public class IOExecutor {
    public IOResult execute() {
        throw new UnsupportedOperationException();
    }
}
