package com.unit6.easen.tester.domain.results.operation;

import java.util.Map;

/**
 * Created by User on 11.09.2014.
 */
public class FIO_Lat {

    private float min;
    private float max;
    private float mean;
    private float stddev;
    private Map<String,Float> percentile;

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public float getMean() {
        return mean;
    }

    public void setMean(float mean) {
        this.mean = mean;
    }

    public float getStddev() {
        return stddev;
    }

    public void setStddev(float stddev) {
        this.stddev = stddev;
    }

    public Map<String, Float> getPercentile() {
        return percentile;
    }

    public void setPercentile(Map<String, Float> percentile) {
        this.percentile = percentile;
    }
}
