package com.unit6.easen.tester.domain.executor;

public class BlockSize {

    public static BlockSize _1024KiB = new BlockSize(1024, Units.KiB);
    public static BlockSize _128KiB = new BlockSize(128, Units.KiB);
    public static BlockSize _64KiB = new BlockSize(64, Units.KiB);
    public static BlockSize _32KiB = new BlockSize(32, Units.KiB);
    public static BlockSize _16KiB = new BlockSize(16, Units.KiB);
    public static BlockSize _8KiB = new BlockSize(8, Units.KiB);
    public static BlockSize _4KiB = new BlockSize(4, Units.KiB);
    public static BlockSize _05KiB = new BlockSize(0.5f, Units.KiB);


    public enum Units {
        KiB
    }

    private final float blockSize;
    private final Units units;

    public BlockSize(float blockSize, Units units) {
        if (units == null) {
            throw new IllegalArgumentException("units can not be null");
        }
        this.blockSize = blockSize;
        this.units = units;
    }

    public float getBlockSize() {
        return blockSize;
    }

    public Units getUnits() {
        return units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlockSize)) return false;

        BlockSize blockSize1 = (BlockSize) o;

        if (Float.compare(blockSize1.blockSize, blockSize) != 0) return false;
        if (units != blockSize1.units) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (blockSize != +0.0f ? Float.floatToIntBits(blockSize) : 0);
        result = 31 * result + units.hashCode();
        return result;
    }
}
