package com.unit6.easen.tester.domain.executor;


public class IOPSTestExecutor {

    public void execute(){
        TestEnvironment testEnvironment = new TestEnvironment();
        //
        WorkloadIndependentPC workloadIndependentPC = new WorkloadIndependentPC();
        TestParameter wipcParams = new TestParameter();
        wipcParams.setUseVolatileCache(false);
        wipcParams.setOIOPerThread(4);
        wipcParams.setThreadCount(2);
        wipcParams.setDataPattern(TestParameter.DataPattern.random);
        workloadIndependentPC.setTestParameters(wipcParams);
        workloadIndependentPC.setCapacity(testEnvironment.getUserCapacity()*2l);
        workloadIndependentPC.execute();

        Workload workload = new Workload();
        workload.setTestParametrs(wipcParams);
        workload.execute();
        //publish results
    }

}
