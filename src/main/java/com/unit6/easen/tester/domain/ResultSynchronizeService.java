package com.unit6.easen.tester.domain;

import com.unit6.easen.tester.domain.results.TestResult;
import com.unit6.easen.tester.infrastructure.SSDTesterServerImpl;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.*;

public class ResultSynchronizeService {

    private ResultsStorage resultsStorage;
    private SSDTesterServer server;
    private long initialDelay;
    private long delay;
    private TimeUnit timeUnit;
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thr = new Thread(r);
            thr.setDaemon(true);
            return thr;
        }
    });

    public ResultSynchronizeService(ResultsStorage resultsStorage, SSDTesterServer server, long initialDelay, long delay, TimeUnit timeUnit) {
        this.resultsStorage = resultsStorage;
        this.server = server;
        this.initialDelay = initialDelay;
        this.delay = delay;
        this.timeUnit = timeUnit;
        this.scheduledExecutorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronizeNow();
                } catch (ServerInteractionException e) {
                    e.printStackTrace();
                }
            }
        }, this.initialDelay, this.delay, this.timeUnit);
    }


    public void newResultEvent() {
        try {
            synchronizeNow();
        } catch (ServerInteractionException e) {
            e.printStackTrace();
        }
    }


    public synchronized void synchronizeNow() throws ServerInteractionException {
        System.out.println("start sync");
        List<TestResult> allResults = resultsStorage.getAllResults();
        if (allResults.isEmpty()) {
            System.out.println("finish sync, no results");
            return;
        }
        System.out.println("now in db " + allResults.size() + " not sync. results");
        server.login();
        System.out.println("successfully login");

        for (TestResult result : allResults) {
            server.send(result);
            resultsStorage.removeResult(result);
            System.out.println("successfully sent result:" + result.toString());
        }

        System.out.println("successfully sent all data");
        server.logout();
        System.out.println("successfully logout, see results in personal cabinet");
        return;
    }


}
