package com.unit6.easen.tester.domain;

public class ServerInteractionException extends Exception {
    public ServerInteractionException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
