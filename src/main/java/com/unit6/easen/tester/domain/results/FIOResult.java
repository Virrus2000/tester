package com.unit6.easen.tester.domain.results;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class FIOResult implements Serializable {
    @JsonProperty(value = "fio version")
    private String fio_version;
    private List<FIOJob> jobs;


    public FIOResult() {
    }

    public String getFio_version() {
        return fio_version;
    }

    public void setFio_version(String fio_version) {
        this.fio_version = fio_version;
    }

    public List<FIOJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<FIOJob> jobs) {
        this.jobs = jobs;
    }
}
