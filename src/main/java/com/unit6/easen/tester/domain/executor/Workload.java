package com.unit6.easen.tester.domain.executor;


import java.util.ArrayList;
import java.util.List;

public class Workload {

    private TestParameter testParametrs;

    public void setTestParametrs(TestParameter testParametrs) {
        this.testParametrs = testParametrs;
    }

    public enum Process {
        DependentPreconditioning, MeasurementWindow
    }

    public void execute() {

        List<RWMix> rwMixes = new ArrayList<>();
        rwMixes.add(RWMix.read(100));
        rwMixes.add(RWMix.read(95));
        rwMixes.add(RWMix.read(65));
        rwMixes.add(RWMix.read(50));
        rwMixes.add(RWMix.read(35));
        rwMixes.add(RWMix.read(5));
        rwMixes.add(RWMix.read(0));
        List<BlockSize> blockSizes = new ArrayList<>();
        blockSizes.add(BlockSize._1024KiB);
        blockSizes.add(BlockSize._128KiB);
        blockSizes.add(BlockSize._64KiB);
        blockSizes.add(BlockSize._32KiB);
        blockSizes.add(BlockSize._16KiB);
        blockSizes.add(BlockSize._8KiB);
        blockSizes.add(BlockSize._4KiB);
        blockSizes.add(BlockSize._05KiB);

        int roundCount = 0;
        int maxRounds = 25;
        int measurementWindowRounds = 4;
        int measurementWindowRoundsNow = 4;
        Process process;
        process = Process.DependentPreconditioning;
        boolean finishing = false;
        while (true) {
            for (RWMix rwMix : rwMixes) {
                for (BlockSize blockSize : blockSizes) {
                    IOExecutor executor = new IOExecutor();
                    IOResult execute = executor.execute();
                    if (rwMix.equals(RWMix.write(100)) && blockSize.equals(BlockSize._4KiB)) {
                        if (process != Process.MeasurementWindow && isSteadyState(execute)) {
                            process = Process.MeasurementWindow;

                        }
                    }
                    if (process == Process.MeasurementWindow){
                        measurementWindowRoundsNow++;
                        if (measurementWindowRoundsNow>measurementWindowRounds){
                            finishing = true;
                        }
                    }
                }
            }
            roundCount++;
            if (roundCount > maxRounds && maxRounds > 0) {
                return;
            }
            if (finishing){
                return;
            }
        }
    }

    public boolean isSteadyState(IOResult result) {
        throw new UnsupportedOperationException();
    }
}

