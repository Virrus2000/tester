package com.unit6.easen.tester.domain.results.operation;

/**
 * Created by User on 11.09.2014.
 */
public class FIOOperation {
    long io_bytes;
    int bw;
    int iops;
    int runtime;
    FIO_Lat slat;
    FIO_Lat clat;
    FIO_Lat lat;
    float bw_min;
    float bw_max;
    float bw_agg;
    float bw_mean;
    float bw_dev;

    public long getIo_bytes() {
        return io_bytes;
    }

    public void setIo_bytes(long io_bytes) {
        this.io_bytes = io_bytes;
    }

    public int getBw() {
        return bw;
    }

    public void setBw(int bw) {
        this.bw = bw;
    }

    public int getIops() {
        return iops;
    }

    public void setIops(int iops) {
        this.iops = iops;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public FIO_Lat getSlat() {
        return slat;
    }

    public void setSlat(FIO_Lat slat) {
        this.slat = slat;
    }

    public FIO_Lat getClat() {
        return clat;
    }

    public void setClat(FIO_Lat clat) {
        this.clat = clat;
    }

    public FIO_Lat getLat() {
        return lat;
    }

    public void setLat(FIO_Lat lat) {
        this.lat = lat;
    }

    public float getBw_min() {
        return bw_min;
    }

    public void setBw_min(float bw_min) {
        this.bw_min = bw_min;
    }

    public float getBw_max() {
        return bw_max;
    }

    public void setBw_max(float bw_max) {
        this.bw_max = bw_max;
    }

    public float getBw_agg() {
        return bw_agg;
    }

    public void setBw_agg(float bw_agg) {
        this.bw_agg = bw_agg;
    }

    public float getBw_mean() {
        return bw_mean;
    }

    public void setBw_mean(float bw_mean) {
        this.bw_mean = bw_mean;
    }

    public float getBw_dev() {
        return bw_dev;
    }

    public void setBw_dev(float bw_dev) {
        this.bw_dev = bw_dev;
    }
}
