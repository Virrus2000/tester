package com.unit6.easen.tester.domain.executor;

public class TestParameter {

    public enum DataPattern {
        random
    }

    private boolean useVolatileCache;
    private int OIOPerThread;
    private int threadCount;
    private DataPattern dataPattern;

    public void setDataPattern(DataPattern dataPattern) {
        this.dataPattern = dataPattern;
    }

    public void setOIOPerThread(int OIOPerThread) {
        this.OIOPerThread = OIOPerThread;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public void setUseVolatileCache(boolean useVolatileCache) {
        this.useVolatileCache = useVolatileCache;
    }

}

