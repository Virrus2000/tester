package com.unit6.easen.tester.view;

import com.unit6.easen.Tester;
import com.unit6.easen.tester.application.TaskExecutorService;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class BaseController implements Initializable {

    public TextArea task;
    public TextField command;
    public TextArea output;
    public static final String TASK_FILE_NAME = "task.ini";


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Console console = new Console(output);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        String taskContent;
        try {
            taskContent = FileUtils.readFileToString(new File(TASK_FILE_NAME));
        } catch (IOException e) {
            taskContent = "task not found, your can type it here";
        }
        File taskFile =  new File(TASK_FILE_NAME);
        task.setText(taskContent);
        command.setText(Tester.config.getTaskExecutorService().getCommandText(taskFile));
        System.out.println(Tester.config.getSsdTesterServer().getParams());
    }

    public void performTest(ActionEvent actionEvent) {
        String taskText = task.getText();
        System.out.println(task.getText());
        File newFile = new File(TASK_FILE_NAME);
        try {
            FileOutputStream fooStream = null; // true to append
            fooStream = new FileOutputStream(newFile, false);
            byte[] myBytes = taskText.getBytes();
            fooStream.write(myBytes);
            fooStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Tester.config.getTaskExecutorService().executeFIOTask(newFile);
    }


}
