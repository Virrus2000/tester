package com.unit6.easen.tester.view;


import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.*;

public class Console {
    private final TextArea output;

    private PipedInputStream pis;

    public Console(TextArea ta) throws IOException {
        this.output = ta;
        pis = new PipedInputStream();
        PipedOutputStream pos = null;
        pos = new PipedOutputStream(pis);
        PrintStream ps = new PrintStream(pos, true);
        System.setOut(ps);
        System.setErr(ps);
        final BufferedReader reader = new BufferedReader(new InputStreamReader(pis));
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        final String readLine = reader.readLine();
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                output.appendText(readLine + "\n");
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
        };
        thread.setDaemon(true);
        thread.start();
    }
}