package com.unit6.easen.tester.infrastructure;

import com.unit6.easen.tester.domain.ResultsStorage;
import com.unit6.easen.tester.domain.results.TestResult;

import java.util.ArrayList;
import java.util.List;

public class ResultsStorageImpl implements ResultsStorage {

    private ArrayList<TestResult> testResults = new ArrayList<>();

    @Override
    public List<TestResult> getAllResults() {
        return new ArrayList<>(testResults);
    }

    @Override
    public void addResult(TestResult result) {
        testResults.add(result);
    }

    @Override
    public void removeResult(TestResult result) {
        testResults.remove(result);
    }

    @Override
    public void removeAll() {
        testResults.clear();
    }
}
