package com.unit6.easen.tester.infrastructure;

public class ExecutionException extends Exception {
    public ExecutionException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
