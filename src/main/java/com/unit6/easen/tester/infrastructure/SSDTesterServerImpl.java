package com.unit6.easen.tester.infrastructure;


import com.unit6.easen.tester.domain.SSDTesterServer;
import com.unit6.easen.tester.domain.ServerInteractionException;
import com.unit6.easen.tester.domain.results.TestResult;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.IOException;

public class SSDTesterServerImpl implements SSDTesterServer {

    private static final String OK_URL = "tester/api/result";

    private ResteasyClient client;
    private String username;
    private String secretKey;
    private String host;
    private int port = 80;


    public SSDTesterServerImpl(String username, String secretKey, String host) {
//        SSDTesterServer server = new SSDTesterServerImpl("virrus2000@gmail.com", "123123", "http://localhost:8080");
        this.username = username;
        this.secretKey = secretKey;
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }



    @Override
    public void login() throws ServerInteractionException {

        DefaultHttpClient httpClient = new DefaultHttpClient();

        BasicHttpContext localContext = new BasicHttpContext();
        httpClient.addRequestInterceptor(new PreemptiveAuthInterceptor(), 0);


        BasicScheme basicAuth = new BasicScheme();
        localContext.setAttribute("preemptive-auth", basicAuth);

        ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(httpClient, localContext);


        client = new ResteasyClientBuilder().httpEngine(engine).build();
        System.out.println("connection param installed successfully, login not required");
    }

    class PreemptiveAuthInterceptor implements HttpRequestInterceptor {

        public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
            AuthState authState = (AuthState) context.getAttribute(ClientContext.TARGET_AUTH_STATE);


            if (authState.getAuthScheme() == null) {
                AuthScheme authScheme = (AuthScheme) context.getAttribute("preemptive-auth");
                if (authScheme != null) {
                    Credentials creds = new UsernamePasswordCredentials(username, secretKey);
                    if (creds == null) {
                        throw new HttpException("No credentials for preemptive authentication");
                    }
                    authState.setAuthScheme(authScheme);
                    authState.setCredentials(creds);
                }
            }
        }
    }

    @Override
    public void send(TestResult result) throws ServerInteractionException {
        try {
            ResteasyWebTarget target = client.target(host + OK_URL);
            Response post = target.request().post(Entity.json(result));
            System.out.println("response:" + post.readEntity(String.class) + " " + post.getLength() + " " + post.getStatus());
        } catch (Exception ex) {
            throw new ServerInteractionException("error on send", ex);
        }
    }


    @Override
    public void logout() throws ServerInteractionException {
    }

    @Override
    public String getParams() {
        return username + "|" + secretKey + "|" + host;
    }
}
