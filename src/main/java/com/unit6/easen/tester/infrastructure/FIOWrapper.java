package com.unit6.easen.tester.infrastructure;

import com.unit6.easen.tester.domain.results.FIOResult;
import com.unit6.easen.tester.domain.results.TestResult;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.util.ArrayList;


public class FIOWrapper {


    private File fioExecutable;
    private File task;
    private boolean readOnly = true;

    public FIOWrapper() {
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public FIOWrapper(File fioExecutable, File task) {
        this.fioExecutable = fioExecutable;
        this.task = task;
    }

    public String execute() throws ExecutionException{
        Runtime runtime = Runtime.getRuntime();
        String json;
        try {

            Process fio = runtime.exec(getArgs());
            InputStream inputStream = fio.getInputStream();
            json = readIS(inputStream);
            fio.waitFor();
            return json;
        } catch (IOException | InterruptedException e) {
            throw new ExecutionException("unknown exception while execute fio", e);
        }

//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            FIOResult result = mapper.readValue(json, FIOResult.class);
//            return result;
//        } catch (IOException e) {
//            throw new IncorrectOutputFormat("cant parse results to TestResult.class", json);
//        }
    }

    private String[] getArgs() {
        ArrayList<String> args = new ArrayList<String>();
        args.add(fioExecutable.getAbsolutePath());
        if (readOnly) {
            args.add("--readonly");
        }
        args.add("--output-format=json");
        args.add(task.getAbsolutePath());
        return args.toArray(new String[args.size()]);
    }

    public void setFioExecutable(File fioExecutable) {
        this.fioExecutable = fioExecutable;
    }

    public void setTask(File task) {
        this.task = task;
    }

    public static String readIS(final InputStream stream) throws IOException {
        StringBuffer result = new StringBuffer();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        String line;
        do {
            line = bufferedReader.readLine();
            System.out.println(line);
            if (line != null) {
                result.append(line);
            }
        } while (line != null);
        return result.toString();
    }

    public String getCommandText() {
        StringBuilder builder = new StringBuilder();
        for (String s : getArgs()) {
            builder.append(" ");
            builder.append(s);
        }
        return builder.toString();
    }
}