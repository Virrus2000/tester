package com.unit6.easen.tester.infrastructure;

public class IncorrectOutputFormat extends Exception {

    private String output;

    public IncorrectOutputFormat(String message, String output) {
        super(message);
        this.output = output;
    }

    public String getOutput() {
        return output;
    }
}
