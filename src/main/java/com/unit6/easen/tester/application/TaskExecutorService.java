package com.unit6.easen.tester.application;

import com.unit6.easen.tester.domain.ResultSynchronizeService;
import com.unit6.easen.tester.domain.ResultsStorage;
import com.unit6.easen.tester.domain.results.FIOResult;
import com.unit6.easen.tester.domain.results.TestResult;
import com.unit6.easen.tester.infrastructure.ExecutionException;
import com.unit6.easen.tester.infrastructure.FIOWrapper;
import com.unit6.easen.tester.infrastructure.IncorrectOutputFormat;

import java.io.File;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskExecutorService {

    private ExecutorService executorService = Executors.newFixedThreadPool(1);
    private ResultSynchronizeService resultSynchronizeService;
    private ResultsStorage resultsStorage;

    public TaskExecutorService(ResultsStorage resultsStorage, ResultSynchronizeService resultSynchronizeService) {
        this.resultsStorage = resultsStorage;
        this.resultSynchronizeService = resultSynchronizeService;
    }

    public void executeFIOTask(File taskFile) {
        final FIOWrapper wrapper = createWrapper(taskFile);
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                preformTest(wrapper);
            }
        });
    }

    public String getCommandText(File taskFile) {
        return createWrapper(taskFile).getCommandText();
    }

    private FIOWrapper createWrapper(File taskFile) {
        FIOWrapper wrapper = new FIOWrapper();
        wrapper.setFioExecutable(new File("fio.exe"));
        wrapper.setTask(taskFile);
        wrapper.setReadOnly(false);
        return wrapper;
    }

    private void preformTest(FIOWrapper wrapper) {
        Date startTime = new Date();
        TestResult testResult = new TestResult(startTime);
        try {
            String execute = wrapper.execute();
            testResult.addNewResult(execute);
            System.out.println("fio test successfully  executed");
        } catch (ExecutionException ex) {
            testResult.addNewResult(ex.getMessage());
            System.out.println("fio test failed");
            ex.printStackTrace();
        }
        testResult.setTestDuration(new Date().getTime() - startTime.getTime());
        resultsStorage.addResult(testResult);
        resultSynchronizeService.newResultEvent();
    }
}
