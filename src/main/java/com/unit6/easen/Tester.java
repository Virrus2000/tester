package com.unit6.easen;

import com.unit6.easen.tester.application.TaskExecutorService;
import com.unit6.easen.tester.domain.ResultSynchronizeService;
import com.unit6.easen.tester.domain.ResultsStorage;
import com.unit6.easen.tester.domain.SSDTesterServer;
import com.unit6.easen.tester.infrastructure.ResultsStorageImpl;
import com.unit6.easen.tester.infrastructure.SSDTesterServerImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import jwrapper.JWParameteriser;
import jwrapper.JWStreamParameteriser;
import jwrapper.jwutils.JWSystem;
import jwrapper.updater.JWLaunchProperties;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Tester extends Application {

    public static Configuration config = new Configuration();


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

    }

    public static void some() {
        String appfile = "jwrapper/build/Tester-windows64-offline.exe";
        try {
            Properties orig = new JWParameteriser().getParameters(new File(appfile));
            Properties custom = (Properties) orig.clone();
            custom.setProperty("user", "111");
            custom.setProperty("secretKey", "222");
            custom.setProperty("host", "333");
            JWStreamParameteriser sp = new JWParameteriser().newStreamParameteriser(custom);

            InputStream in = new BufferedInputStream(new FileInputStream(appfile));
            byte[] buf = new byte[20000];
            int n = 0;
            File file = new File("temp.exe");
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            while (n != -1) {
                n = in.read(buf);
                if (n > 0) {
                    //JWStreamParameteriser will replace bytes as necessary
                    sp.nextBlockToBeTransferred(buf, 0, n);
                    fileOutputStream.write(buf, 0, n);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
//        some();
        String user = "virrus2000@gmail.com";
        String secretKey = "uICJH54V6u1WZAg3GkJYGJtQ1dfMrP2v";
        String host = "http://ec2-54-85-133-225.compute-1.amazonaws.com/";
        if (JWLaunchProperties.isJWrapperSetup()) {
            user = JWSystem.getAppLaunchProperty("user");
            secretKey = JWSystem.getAppLaunchProperty("secretKey");
            host = JWSystem.getAppLaunchProperty("host");
        }
        config.initialize(user, secretKey, host, 1);
        launch(args);
    }


    public static class Configuration {

        private TaskExecutorService taskExecutorService;
        private ResultsStorage resultsStorage;
        private ResultSynchronizeService resultSynchronizeService;
        private SSDTesterServer ssdTesterServer;

        public void initialize(String username, String secretKey, String serverHost, int delayInMinutes) {
            resultsStorage = new ResultsStorageImpl();
            ssdTesterServer = new SSDTesterServerImpl(username, secretKey, serverHost);
            resultSynchronizeService = new ResultSynchronizeService(resultsStorage, ssdTesterServer, delayInMinutes, delayInMinutes, TimeUnit.MINUTES);
            taskExecutorService = new TaskExecutorService(resultsStorage, resultSynchronizeService);
        }

        public SSDTesterServer getSsdTesterServer() {
            return ssdTesterServer;
        }

        public TaskExecutorService getTaskExecutorService() {
            return taskExecutorService;
        }
    }
}
